package main

import (
	"fmt"

	"github.com/gocolly/colly"
)

func main() {
	// TODO: get this as input
	purl := "http://localhost:3000"
	// u, err := url.Parse(purl)
	// if err != nil {
	// 	panic(err)
	// }

	// Instantiate default collector
	c := colly.NewCollector(
	// Visit only domains of the purl
	// if I set this, then redirects are not followed
	// colly.AllowedDomains(u.Host),
	)

	// On every a element which has href attribute call callback
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		// Print link
		fmt.Printf("Link found: %q -> %s\n", e.Text, link)
		// Visit link found on page
		// Only those links are visited which are in AllowedDomains
		c.Visit(e.Request.AbsoluteURL(link))
	})

	// c.OnResponse(func(r *colly.Response) {
	// 	fmt.Println("got back: ", r.StatusCode, " for: ", r.Request.URL)
	// 	fmt.Println("header: ", r.Headers)
	// })

	// Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnError(func(r *colly.Response, err error) {
		fmt.Println("Request URL:", r.Request.URL, "\nError:", err)
		fmt.Println("Err Code: ", r.StatusCode)
	})

	c.Visit(purl)
}
